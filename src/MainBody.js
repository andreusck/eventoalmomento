import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container } from 'reactstrap';

import Home from './Home';
import Alertas from './Alertas/Alertas';
import Eventos from "./Eventos"
import MapaEvento from "./MapaEvento"
import MapaMultiple from "./MapaMultiple"
import Transporte from "./Transporte/Transporte";
import ReEntradas from "./ReEntradas/ReEntradas";
import Admin from "./Admin";
import Elimina from "./Elimina";
import EventoUnico from "./EventoUnico"
import Edita from "./Edita"
import Nosotros from "./Nosotros/Nosotros"



class MainBody extends React.Component {


    render() {

        return (
            <>

                <h1 id="idMainBodyTXT"></h1>

             
                    <Switch>
                        <Route exact path="/" component={Eventos} />
                        <Route exact path="/MapaMultiple" component={MapaMultiple} />
                        <Route path="/Alertas/:idEvento" component={Alertas} />
                        <Route path="/Transporte/:idEvento" component={Transporte} />
                        <Route path="/ReEntradas/:idEvento" component={ReEntradas} />
                        <Route exact path="/Admin" component={Admin} />
                        <Route path="/Admin/mapamultiple" component={MapaMultiple} />
                        <Route path="/Elimina/:idEvento" component={Elimina} />
                        <Route path="/Mapa/:idEvento" render={(props)=><MapaEvento editable={true} {...props} />} />
                        <Route path="/MapaND/:idEvento" render={(props)=><MapaEvento editable={false} {...props} />} />
                        <Route path="/Evento/:idEvento" component={EventoUnico} />
                        <Route path="/Admin/Edita/:idEvento" component={Edita} />
                        <Route path="/Nosotros" component={Nosotros} />
                    </Switch>
                


            </>
        )
    }
}


export default MainBody;