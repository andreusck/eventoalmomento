import React from "react";
import { URL,IMGURL } from "../Utils";
import "./transporte.css";
import NuevoComen from "./NuevoComen";
import Mouse from "./../Fotos/mouse.png"
import Fox from "./../Fotos/fox.png"
import Frog from "./../Fotos/frog.png"
const AVATARS = [Mouse, Fox, Frog];

class Mostrar_comen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productos: null,
      nombreEvento:null
    };

    this.cargaDatos = this.cargaDatos.bind(this);
  }

  componentDidMount() {

    this.cargaDatos();
  }

  cargaDatos(){
    fetch(URL + `evm_comentarios?_size=100&_where=(categoria,eq,2)~and(idevento,eq,${this.props.eventoActual})`)
      .then(data => data.json())
      .then(data => this.setState({ productos: data }))
      .catch(err => console.log(err));

      fetch(URL + `evm_eventos/${this.props.eventoActual}`)
      .then(data => data.json())
      .then(data => this.setState({ nombreEvento: data[0].titulo, eventoEntero: data[0]}))
      .catch(err => console.log(err));
  }

  render() {
    if (this.state.productos===null) {
      return <h4>Cargando datos...</h4>;
    }



    let rows = this.state.productos.map(el => {

      
      let fechaNueva = el.fecha_hora.split("T")[0];
      let horaNuevaRaw = el.fecha_hora.split("T")[1];
      let horaNueva = horaNuevaRaw.split(".")[0];
    

      if (el.nivel===1){
      
          el.nivel = <div className="nivel1_t"></div>
      
      }

      if (el.nivel===2){
      
          el.nivel=<div className="nivel2_t"></div>
     
      }
      
      if (el.nivel===3){
       
          el.nivel=<div className="nivel3_t"></div>
 
      }

      return (
        <tr key={el.id}>
          <td>{fechaNueva} / {horaNueva}</td>
          <td>{el.nombre}</td>
          <td><img src={AVATARS[el.icono - 1]} width="50px" /></td>
          <td>{el.texto}</td>
          <td>{el.nivel}</td>
          
          

        </tr>
      );
    });

    let opcionNuevoComentario = (
      <NuevoComen
        eventoActual={this.props.eventoActual}
        cargaDatos={this.cargaDatos}
      />
    );;
    let hoy = (new Date()).toJSON();
    console.log(this.state.eventoEntero, hoy)

    if (this.state.eventoEntero && this.state.eventoEntero.fecha<hoy){
      opcionNuevoComentario = <></>;
    }

    return (
      <>
        <h1 className="titulocomen">TRANSPORTE:  {this.state.nombreEvento}</h1>
        <table className="table">
        <thead>
        <tr>
          <th>Fecha Hora</th>
          <th>Usuario</th>
          <th>Icono</th>
          <th>Texto</th>
          <th>Disponibilidad</th>
        </tr>
        </thead>
          <tbody>{rows}</tbody>
        </table>
        
        {opcionNuevoComentario}

      </>
    );
  }
}

export default Mostrar_comen;