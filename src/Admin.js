import React from "react";
import { URL, IMGURL } from "./Utils";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import AdminEdit from "./AdminEdit";
import { Alert } from "reactstrap";
import "./estilos.css";
class Admin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      titulo: "",
      fecha: "",
      hora: "",
      texto: "",
      nombreFoto: "",
      volver: false,
      alerta: false,
      cssBodyVisibility: "none",
      cssPassVisibility: "block",
      passtemp: "",
      pass: "eventoalmomento",
      alertaState: false,
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.volver = this.volver.bind(this);
    this.mostrarBody = this.mostrarBody.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  onSubmit(e) {
    e.preventDefault();

    if (
      this.state.titulo == false ||
      this.state.fecha == false ||
      this.state.hora == false ||
      this.state.texto == false ||
      this.state.nombreFoto == false
    ) {
      this.setState({ alerta: true });
    } else {
      console.log(this.state.texto);
      let comentario = {
        titulo: this.state.titulo,
        texto: this.state.texto,
        nombreFoto: this.state.nombreFoto,
        fecha: this.state.fecha,
        hora: this.state.hora,
      };

      fetch(URL + "evm_eventos", {
        method: "POST",
        headers: new Headers({ "Content-Type": "application/json" }),
        body: JSON.stringify(comentario)
      })
        .then(data => data.json())
        .then(data => console.log(data))
        .then(() =>
          this.setState({ nombre: "", texto: "", titulo: "", nombreFoto: "", fecha:"", hora:"" })
        )
        // .then(()=>this.props.cargaDatos())
        .catch(err => console.log(err));
    }
  }

  volver() {
    this.setState({ volver: true });
  }
  mostrarBody() {
    this.setState({ 
      alertaState: false,
   });
    if (this.state.passtemp == this.state.pass) {
      this.setState({ 
        cssPassVisibility: "none",
        cssBodyVisibility: "block",
      
     });
    }
    else {
      this.setState({ 
        alertaState: true,
     });
    }
  }

  render() {
    let msgAlerta = <></>;
    if (this.state.alerta) {
      msgAlerta = (
        <Alert color="warning">
          <h4 className="alert-heading">RELLENE TODO EL FORMULARIO PARA ENVIAR EL EVENTO, GRACIAS!</h4>
        </Alert>
      );
    }

    let bodyAdmin = {
      display: this.state.cssBodyVisibility
    }
    let cssPass = {
      display: this.state.cssPassVisibility
    }
    let msgAlertaPassWrong = <></>;
    if (this.state.alertaState) {
      msgAlertaPassWrong = (
        <Alert color="warning">
          <h4 className="alert-heading">Password incorrecto</h4>
        </Alert>
      );
    }

    return (
      <>
      {msgAlertaPassWrong}
      <div className="text-center adminPassword" style={cssPass}>
        Introduzca el código de acceso
       
        <br></br><input type="text"
        className="adminInput adminInputTitulo"
        onChange={this.handleInputChange}
        value={this.state.passtemp}
        name="passtemp"
        placeholder="Password"></input>
        
        <input className="BotoncitoENVIAR" type="submit" value="Enviar" onClick={this.mostrarBody} />
        
      </div>


      <div style={bodyAdmin}>

      
        <h1 className="tituloalerta">Envie el Evento</h1>
        <hr />
        {msgAlerta}
        <form onSubmit={this.onSubmit}>
          <input
            className="adminInput adminInputTitulo"
            type="text"
            onChange={this.handleInputChange}
            value={this.state.titulo}
            name="titulo"
            placeholder=" Título"
          />

          <input
            className="adminInput"
            type="url"
            onChange={this.handleInputChange}
            value={this.state.nombreFoto}
            name="nombreFoto"
            placeholder=" URL Foto"
          />

          <input
            className="adminInput"
            type="date"
            onChange={this.handleInputChange}
            value={this.state.fecha}
            name="fecha"
            placeholder="fecha"
          />
          <input
            className="adminInput "
            type="time"
            onChange={this.handleInputChange}
            value={this.state.hora}
            name="hora"
            placeholder="hora"
          />
          <textarea
            className="adminInput adminInputDescripcion"
            type="text"
            onChange={this.handleInputChange}
            value={this.state.texto}
            name="texto"
            placeholder=" Descripción"
          />

          <br></br>

          <input className="BotoncitoENVIAR" type="submit" value="Enviar" />
        </form>
        <AdminEdit />
        </div>
      </>
    );
  }
}

export default Admin;
