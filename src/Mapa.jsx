import React, { createRef, Component } from "react";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import Leaflet from "leaflet";
//carreguem css leaflet
import "leaflet/dist/leaflet.css";

//definició icones, necessari en versió actual de leaflet amb react...
//si no no apareix la icona del Marker
Leaflet.Icon.Default.imagePath = "../node_modules/leaflet";
delete Leaflet.Icon.Default.prototype._getIconUrl;
Leaflet.Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png")
});



const getReverseAddressMap = (coords) => {
    let lat = coords.lat || coords[0] || null;
    let lon = coords.lon || coords.lng || coords[1] || null;
   
    return new Promise((resolve, reject) => {
      if (!lat || !lon){
        reject ({error: "nodata"});
      }
   
        fetch(`https://us1.locationiq.com/v1/reverse.php?key=pk.b6318f65a0a482c1076929bb005aa0ae&lat=${lat}&lon=${lon}&format=json`)
        .then(data => data.json())
        .then(data => {
            if (data.error){
              reject(data);
            }else{
              resolve(data);
            }
            resolve(data);
        })
        .catch(error => {
            reject({error});
        });
    });
  }






class Mapa extends Component {

  constructor(props){
    super(props);

    this.mapRef = createRef();
    this.markerRef = createRef();
    this.locatePosition = this.locatePosition.bind(this);

  }


  locatePosition = () => {
    const map =this.mapRef.current;
    const currentZoom = map.leafletElement.getZoom();

    const marker = this.markerRef.current;
    let coords = marker.leafletElement.getLatLng();


    if (marker != null) {
      getReverseAddressMap(coords)
      .then(position => {
        this.props.setCurrent(position, currentZoom);
      })
      .catch(err => console.log(err));
    }
  }


  render() {
      let posicio = [this.props.current.lat, this.props.current.lon];
      let popupContent = this.props.popupContent || this.props.current.display_name;

    return (
      <>
        <Map
          ref={this.mapRef}
          center={posicio}
          zoom={this.props.zoom}
          style={{ height: this.props.sizex, width: this.props.sizey }}
        >
          <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        
          <Marker position={posicio}
          draggable={this.props.editable}
          onDragend={this.locatePosition}
          ref={this.markerRef}
          >
            
          <Popup>
              {popupContent}
          </Popup>
        </Marker>

        </Map>
      </>
    );
  }
}

export default Mapa;
