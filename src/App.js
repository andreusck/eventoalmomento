import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container } from 'reactstrap';

import TopBar from './TopBar'
import Cabecera from './Cabecera'
import Lateral from './Lateral'
import MainBody from "./MainBody"
import FootBar from "./FootBar"

import './estilos.css';

export default () => (
  <BrowserRouter>
    <Container>
      <TopBar />
      <div className="marginTopPrincipal">


          <MainBody className="lineall" />
      </div>
     
    </Container>
   
  </BrowserRouter>
);
