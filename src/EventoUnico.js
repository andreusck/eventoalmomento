import React from "react";
import { URL, IMGURL } from "./Utils";
import { BrowserRouter, Link, Switch, Route, Redirect } from "react-router-dom";
import { Container } from "reactstrap";
import { Row, Button, ButtonGroup, Fade } from "reactstrap";

class EventoUnico extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            difuminarTexto1A: "block",
            difuminarTexto1B: "none",
            difuminarTexto2A: "block",
            difuminarTexto2B: "none",
            informacion: [],
            numAlertas: 0,
            infoEvento: null,
            id: this.props.match.params.idEvento,

        };
        this.botonTexto1 = this.botonTexto1.bind(this);
        this.cargaDatos = this.cargaDatos.bind(this);
        this.conmutador = this.conmutador.bind(this);
    }
    cargaDatos() {
        console.log("cargando datos...");
        fetch(
            URL +
            `evm_comentarios/count?_where=(categoria,eq,1)~and(idevento,eq,${this.state.id})`
        )
            .then(data => data.json())
            .then(data => this.setState({ numAlertas: data[0].no_of_rows }))
            .catch(err => console.log(err));

        fetch(
            URL +
            `evm_comentarios/count?_where=(categoria,eq,2)~and(idevento,eq,${this.state.id})`
        )
            .then(data => data.json())
            .then(data => this.setState({ numEntradas: data[0].no_of_rows }))
            .catch(err => console.log(err));

        fetch(
            URL +
            `evm_comentarios/count?_where=(categoria,eq,3)~and(idevento,eq,${this.state.id})`
        )
            .then(data => data.json())
            .then(data => this.setState({ numTransporte: data[0].no_of_rows }))
            .catch(err => console.log(err));

        fetch(URL + `evm_eventos/${this.state.id}`)
            .then(data => data.json())
            .then(data => {
                this.setState({
                    infoEvento: data[0],
                    linkFoto: data[0].nombreFoto,
                    HoraEvento: data[0].hora.substring(0, 5),
                    FechaEvento: data[0].fecha.split("T")[0],
                    tituloActual: data[0].titulo,
                    textoActual: data[0].texto,
                });
            })
            .catch(err => console.log(err));
    }
    componentDidMount() {
        this.cargaDatos();
    }





    conmutador() {
        if (this.state.difuminarTexto2A == "block") {
            this.setState({
                difuminarTexto2A: "none",
                difuminarTexto2B: "block"
            });
        } else {
            this.setState({
                difuminarTexto2A: "block",
                difuminarTexto2B: "none"
            });
        }
    }
    botonTexto1() {
        if (this.state.difuminarTexto1A == "block") {
          this.setState({
            difuminarTexto1A: "none",
            difuminarTexto1B: "block"
          });
        } else {
          this.setState({
            difuminarTexto1A: "block",
            difuminarTexto1B: "none"
          });
        }
      }

    render() {

        let estiloTexto2A = {
            display: this.state.difuminarTexto2A
        };
        let estiloTexto2B = {
            display: this.state.difuminarTexto2B
        };

        if (!this.state.infoEvento) {
            return <h4>Cargando datos...</h4>;
        }
        return (
            <>
        <Row className="mb-5">
          <div className="col-12 col-md-6  mb-3">
            <div className="cssEvento">
              {/* {this.state.tituloActual} */}
              <img
                className="img-fluid cssFotoDelEvento "
                src={this.state.linkFoto}
              />
              <div className="cssOpcionesMapa">
                  
                   <Link to={"/mapaND/" + this.state.id}  ><img  className="iconoMapa" src="https://i.imgur.com/Q7XZW1l.png"></img></Link>
              </div>
              <div className="OpcionesEvento">
                <Button className="BotonOPCIONES" onClick={this.conmutador}>
                  Opciones Evento
                </Button>

              </div>
            </div>
          </div>
          <div className="col-12 col-md-6 ">
            <div id="texto1" style={estiloTexto2A}>
            <div className="TITULO">
            <strong>{this.state.tituloActual}</strong>
            </div>
            {this.state.textoActual}
            <br></br>
            <u>Hora y Fecha del Evento:</u>
            <br></br>
            {(this.state.HoraEvento) ? (this.state.HoraEvento) : "Hora no definida" } / {(this.state.FechaEvento) ? (this.state.FechaEvento) : "Fecha no definida"}
              
            </div>


            <div id="texto2" style={estiloTexto2B}>
              <div className="eventoBoton1">
                <Link
                  className="nav-link" 
                  to={"/Alertas/" + this.state.id}
                >
                  ALERTAS
                  <div className="botonmensajes">
                    
                      
                      
                       {this.state.numAlertas}
                      
                      
                    
                  </div>
                </Link>
              </div>

              <div className="eventoBoton1">
                <Link
                  className="nav-link"
                  to={"/Transporte/" + this.state.id}
                >
                  TRANSPORTE
                  <div className="botonmensajes">
                    
                        {"  "  +  this.state.numEntradas}
                    
                  </div>
                </Link>
              </div>
              <div className="eventoBoton1">
                <Link
                  className="nav-link"
                  to={"/ReEntradas/" + this.state.id}
                >
                  ENTRADAS
                  <div className="botonmensajes">
                   
                        {+this.state.numTransporte}
                   
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </Row>
            </>
        );
    }
}

export default EventoUnico;