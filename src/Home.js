import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import Carrusel from "./Carrusel"
import Eventos from "./Eventos"

class Home extends React.Component {

    render() {

        return (
            <>
                <div className="cssMarginBotom">
                    <Carrusel />
                </div>
                <Link className="nav-link" to="/Alertas">Alertas</Link>
                
                <Eventos />

            </>
        )
    }
}


export default Home;