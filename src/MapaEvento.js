import React from "react";
import { URL, IMGURL } from "./Utils";
import { Redirect } from "react-router-dom";

import "leaflet/dist/leaflet.css";
import { Container, Row, Col, Button } from "reactstrap";

import Mapa from "./Mapa";
const BCN = {
  lat: 41.390205,
  lon: 2.154007
};
const MapSize = ["100%", 400];
const Zoom = 13;

const Coords = props => (
  <div className="coords">
    <h3>
      {props.current.lat},{props.current.lon}
    </h3>
    <h4>{props.current.display_name}</h4>
  </div>
);

class MapaEvento extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      zoom: Zoom,
      current: {
        lat: 41.390205,
        lon: 2.154007,
        display_name: "Barcelona"
      },
      infoEvento: null,
      id: this.props.match.params.idEvento,
      volver: false
    };
    this.setCurrent = this.setCurrent.bind(this);
    this.guardaCoords = this.guardaCoords.bind(this);
  }

  setCurrent(current, zoom) {
    this.setState({ current, zoom });
  }

  guardaCoords() {
    let evento = this.state.infoEvento;
    evento.lat = this.state.current.lat;
    evento.lon = this.state.current.lon;
    evento.ubicacion = this.state.current.display_name;
    evento.fecha = evento.fecha.split("T")[0];

    fetch(URL + "evm_eventos", {
      method: "PUT",
      headers: new Headers({ "Content-Type": "application/json" }),
      body: JSON.stringify(evento)
    })
      .then(data => data.json())
      .then(data => console.log(data))
      .then(() => this.setState({ volver: false }))
      .catch(err => console.log(err));
  }

  componentDidMount() {
    fetch(URL + "evm_eventos/" + this.state.id)
      .then(data => data.json())
      .then(data =>
        this.setState({
          infoEvento: data[0],
          current: {
            lat: data[0].lat || BCN.lat,
            lon: data[0].lon || BCN.lon,
            display_name: data[0].ubicacion
          }
        })
      )
      .catch(err => console.log(err));
  }

  render() {
    if (this.state.volver) {
      return <Redirect to="/Admin" />;
    }
    if (!this.state.infoEvento) {
      return <h4>Cargando datos...</h4>;
    }
    let infoEvento = this.state.infoEvento;

    let boto=<></>;
    if (this.props.editable){
      boto= <Button onClick={this.guardaCoords}>Guarda coordenadas</Button>;
    }

    return (
      <>
        <Container>
          <Row>
            <Col>
              <h2 className="texto_blanco">{this.state.infoEvento.titulo}</h2>
              <hr />
            </Col>
          </Row>

          <Row>
            <Col>
              <Mapa
                editable={this.props.editable}
                setCurrent={this.setCurrent}
                current={this.state.current}
                zoom={this.state.zoom}
                sizex={MapSize[1]}
                sizey={MapSize[0]}
              />
              <hr />
              <Coords current={this.state.current} />
            </Col>
          </Row>
          <br />
         {boto}
          <br />
          <br />
          <br />
        </Container>
      </>
    );
  }
}

export default MapaEvento;
