import React from "react";
import {URL, IMGURL} from '../Utils';
import { Redirect } from "react-router-dom";
import "./ReEntradas.css";

class NuevoComen extends React.Component {

  constructor(props){
    super(props);
    this.state={
      texto: "",
      categoria: "",
      fecha_hora:"",
      icono:1,
      nombre:"",
      nivel:3,
      categoria: 3,

      volver: false,
      
    };

    this.handleInputChange=this.handleInputChange.bind(this);
    this.onSubmit=this.onSubmit.bind(this);
    this.volver=this.volver.bind(this);

  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  onSubmit(e) {
    e.preventDefault();
    console.log(this.state.texto);
    let comentario = {
      texto: this.state.texto,
      icono: this.state.icono*1,
      nombre: this.state.nombre,
      nivel: this.state.nivel*1,
      categoria: this.state.categoria*1,
      idevento: this.props.eventoActual*1,
    }

    fetch(URL+"evm_comentarios", {
        method: 'POST', 
        headers: new Headers({ 'Content-Type': 'application/json' }),
        body: JSON.stringify(comentario)
      })
    .then(data => data.json())
    .then(data => console.log(data))
    .then(() => this.setState({nombre:"", texto:"", icono:1,nivel:1}))
    .then(()=>this.props.cargaDatos())
    .catch(err => console.log(err));
    
  }

  
  volver(){
    this.setState({volver: true});
  }


  render() {

 

    return (
      <>
        <h1 className="tituloalerta">REVENTA ENTRADAS SOBRE EL EVENTO</h1>
        <hr/>
        <form onSubmit={this.onSubmit} >
        <input className ="NOMBREUSUARIO" type="text" onChange={this.handleInputChange} value={this.state.nombre} name="nombre" placeholder="Nombre_usuario" />
          <input className ="COMENTARIO" type="text" onChange={this.handleInputChange} value={this.state.texto} name="texto" placeholder="Escriba su comentario" />
          <br></br>   
          <select className ="INTROICONO" onChange={this.handleInputChange} value={this.state.icono} name="icono">
            <option className="Icono1" value="1">Mouse</option>
            <option className="Icono2" value="2">Fox</option>
            <option className="Icono3" value="3">Frog</option>
            </select>
          <select className="INTRODISPONIBILIDAD" onChange={this.handleInputChange} value={this.state.nivel} name="nivel">
            <option className="Disponibilidad_Total" value="1">  Disponibilidad Total</option>
            <option className="Disponibilidad_Intermitente" value="2">  Disponibilidad Intermitente</option>
            <option className="Disponibilidad_Nula" value="3">  Disponibilidad Nula</option>
            </select> 

          <input className="BotoncitoENVIAR" type="submit" value="Enviar" />
        </form>

      </>
    );
  }
}

export default NuevoComen;