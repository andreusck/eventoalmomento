import React from "react";
import { URL, IMGURL } from './Utils';
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";

class AdminEdit extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      eventoInfo: null,
    };

    this.cargaDatos = this.cargaDatos.bind(this);
  }

  componentDidMount() {

    this.cargaDatos();
  }

  cargaDatos() {
    fetch(URL+`evm_eventos`)
      .then(data => data.json())
      .then(data => this.setState({ eventoInfo: data }))
      .catch(err => console.log(err));

  }





  render() {
    if (this.state.eventoInfo===null) {
      return <h4>Cargando datos...</h4>;
    }

    let rows = this.state.eventoInfo.map(el => {

      return (
        <tr key={el.id}>
          <td>{el.titulo}</td>
          <td> <Link to={"/Admin/Edita/" + el.id}  >Edita</Link> </td>
          <td> <Link to={"/mapa/" + el.id}  >Mapa</Link> </td>
          <td> <Link to={"/elimina/" + el.id}  >Elimina</Link> </td>
        </tr>
      );
    });

    return (
      <>
        <table className="table">
          <thead>
            <tr>
              <th>Evento</th>
              <th>Edita</th>
              <th>Geolocaliza</th>
              <th>Elimina</th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>

      </>
    );
  }
}


export default AdminEdit;