import React from "react";
import { URL, IMGURL } from "./Utils";
import { BrowserRouter, Link, Switch, Route, Redirect } from "react-router-dom";
import { Container } from "reactstrap";
import { Row, Button, ButtonGroup, Fade } from "reactstrap";
import { Alert } from "reactstrap";

class Edita extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            difuminarTexto1A: "block",
            difuminarTexto1B: "none",
            difuminarTexto2A: "block",
            difuminarTexto2B: "none",
            informacion: [],
            numAlertas: 0,
            infoEvento: null,
            id: this.props.match.params.idEvento,
            alerta: false

        };
        this.onSubmit = this.onSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.botonTexto1 = this.botonTexto1.bind(this);
        this.cargaDatos = this.cargaDatos.bind(this);
        this.conmutador = this.conmutador.bind(this);
    }
    cargaDatos() {
        console.log("cargando datos...");
        fetch(
            URL +
            `evm_comentarios/count?_where=(categoria,eq,1)~and(idevento,eq,${this.state.id})`
        )
            .then(data => data.json())
            .then(data => this.setState({ numAlertas: data[0].no_of_rows }))
            .catch(err => console.log(err));

        fetch(
            URL +
            `evm_comentarios/count?_where=(categoria,eq,2)~and(idevento,eq,${this.state.id})`
        )
            .then(data => data.json())
            .then(data => this.setState({ numEntradas: data[0].no_of_rows }))
            .catch(err => console.log(err));

        fetch(
            URL +
            `evm_comentarios/count?_where=(categoria,eq,3)~and(idevento,eq,${this.state.id})`
        )
            .then(data => data.json())
            .then(data => this.setState({ numTransporte: data[0].no_of_rows }))
            .catch(err => console.log(err));

        fetch(URL + `evm_eventos/${this.state.id}`)
            .then(data => data.json())
            .then(data => {
                this.setState({
                    infoEvento: data[0],
                    nombreFoto: data[0].nombreFoto,
                    HoraEvento: data[0].hora.substring(0, 5),
                    FechaEvento: data[0].fecha.split("T")[0],
                    tituloActual: data[0].titulo,
                    textoActual: data[0].texto,
                });
            })
            .catch(err => console.log(err));
    }
    componentDidMount() {
        this.cargaDatos();
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;
        this.setState({
          [name]: value
        });
      }
      onSubmit(e) {
        e.preventDefault();
    
        {
          console.log(this.state.texto);
          let comentario = {
            id: this.props.match.params.idEvento,
            titulo: this.state.tituloActual,
            texto: this.state.textoActual,
            nombreFoto: this.state.nombreFoto,
            fecha: this.state.FechaEvento,
            hora: this.state.HoraEvento,
          };
    
          fetch(URL + "evm_eventos", {
            method: "PUT",
            headers: new Headers({ "Content-Type": "application/json" }),
            body: JSON.stringify(comentario)
          })
            .then(data => data.json())
            // .then(()=>this.props.cargaDatos())
            .then(() => this.setState({ alerta: true }))
            .catch(err => console.log(err));
           
        }
      }





    conmutador() {
        if (this.state.difuminarTexto2A == "block") {
            this.setState({
                difuminarTexto2A: "none",
                difuminarTexto2B: "block"
            });
        } else {
            this.setState({
                difuminarTexto2A: "block",
                difuminarTexto2B: "none"
            });
        }
    }
    botonTexto1() {
        if (this.state.difuminarTexto1A == "block") {
            this.setState({
                difuminarTexto1A: "none",
                difuminarTexto1B: "block"
            });
        } else {
            this.setState({
                difuminarTexto1A: "block",
                difuminarTexto1B: "none"
            });
        }
    }

    render() {
        let msgAlerta = <></>;
        if (this.state.alerta) {
          msgAlerta = (
            <Alert color="warning">
              <h4 className="alert-heading">Actualizado!</h4>
            </Alert>
          );
        }


        let estiloTexto2A = {
            display: this.state.difuminarTexto2A
        };
        let estiloTexto2B = {
            display: this.state.difuminarTexto2B
        };

        if (!this.state.infoEvento) {
            return <h4>Cargando datos...</h4>;
        }
        return (
            <>
            {msgAlerta}
                <form onSubmit={this.onSubmit}>
                    <Row className="mb-5">

                        <div className="col-12 col-md-6  mb-3">
                            <div className="cssEvento">
                                {/* {this.state.tituloActual} */}
                                <img
                                    className="img-fluid cssFotoDelEvento "
                                    src={this.state.nombreFoto}
                                />
                                <div className="cssOpcionesMapa">

                                    <Link to={"/mapaND/" + this.state.id}  ><img className="iconoMapa" src="https://i.imgur.com/Q7XZW1l.png"></img></Link>
                                </div>
                                <div className="OpcionesEvento">
                                    {/* <Button className="BotonOPCIONES" onClick={this.conmutador}>
                                        Opciones Evento
                </Button> */}

                                </div>

                            </div>
                        </div>
                        <div className="col-12 col-md-6 ">
                            <div id="texto1" style={estiloTexto2A}>
                                <div className="TITULO">
                                    <strong>
                                        <input
                                            className="adminInput adminInputTitulo w-100"
                                            type="text"
                                            onChange={this.handleInputChange}
                                            value={this.state.tituloActual}
                                            name="tituloActual"
                                            placeholder=" Título"
                                        />

                                    </strong>
                                </div>
                                <textarea 
                                    rows="6"
                                    className="adminInput adminInputDescripcion  w-100"
                                    type="text"
                                    onChange={this.handleInputChange}
                                    value={this.state.textoActual}
                                    name="textoActual"
                                    placeholder=" Descripción"
                                />
                                
                               
                                
                               


                            </div>


                            <div id="texto2" style={estiloTexto2B}>
                                <div className="eventoBoton1">
                                    <Link
                                        className="nav-link"
                                        to={"/Alertas/" + this.state.id}
                                    >
                                        ALERTAS
                  <div className="botonmensajes">



                                            {this.state.numAlertas}



                                        </div>
                                    </Link>
                                </div>

                                <div className="eventoBoton1">
                                    <Link
                                        className="nav-link"
                                        to={"/Transporte/" + this.state.id}
                                    >
                                        TRANSPORTE
                  <div className="botonmensajes">

                                            {"  " + this.state.numEntradas}

                                        </div>
                                    </Link>
                                </div>
                                <div className="eventoBoton1">
                                    <Link
                                        className="nav-link"
                                        to={"/ReEntradas/" + this.state.id}
                                    >
                                        ENTRADAS
                  <div className="botonmensajes">

                                            {+this.state.numTransporte}

                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </div>

                    </Row>
                    <Row>
                        <div className="col-6">
                        <input
            className="adminInputEdita"
            type="url"
            onChange={this.handleInputChange}
            value={this.state.nombreFoto}
            name="nombreFoto"
            placeholder=" URL Foto"
          />
        
          </div>
          <div className="col-6">
              
          <input
            className="adminInput "
            type="time"
            onChange={this.handleInputChange}
            value={this.state.HoraEvento}
            name="HoraEvento"
            placeholder="hora"
          /> /        
             <input
          className="adminInput"
          type="date"
          onChange={this.handleInputChange}
          value={this.state.FechaEvento}
          name="FechaEvento"
          placeholder="fecha"
        />
         <input className="BotoncitoENVIAR" type="submit" value="Enviar" />
          </div>
                    </Row>

                </form>
            </>
        );
    }
}

export default Edita;