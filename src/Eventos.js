import React, { useState } from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Row, Button, ButtonGroup, Fade } from "reactstrap";
import ModalExample from "./ModalEvento";
import Carrusel from "./Carrusel";
import InfoEvento from "./InfoEvento"
import FootBar from "./FootBar"

import { URL, IMGURL } from "./Utils";

class Eventos extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // eventoActual: 3, //this.props.idevento
      informacion: [],
    };
    this.cargaDatos = this.cargaDatos.bind(this);
    this.cargaInfo = this.cargaInfo.bind(this);

  }

  componentDidMount() {
    this.cargaInfo();
    this.cargaDatos();
  }
  cargaInfo() {
    console.log("cargando datos...")
    fetch(URL + "evm_eventos")
      .then(data => data.json())
      .then(data => this.setState({ informacion: data }))
      .catch(err => console.log(err));

  }

  cargaDatos() {
    console.log("cargando datos...")
    fetch(URL + "evm_comentarios/count?_where=(categoria,eq,1)")
      .then(data => data.json())
      .then(data => this.setState({ numAlertas: data[0].no_of_rows }))
      .catch(err => console.log(err));

    fetch(URL + "evm_comentarios/count?_where=(categoria,eq,2)")
      .then(data => data.json())
      .then(data => this.setState({ numEntradas: data[0].no_of_rows }))
      .catch(err => console.log(err));

    fetch(URL + "evm_comentarios/count?_where=(categoria,eq,3)")
      .then(data => data.json())
      .then(data => this.setState({ numTransporte: data[0].no_of_rows }))
      .catch(err => console.log(err));


  }


  render() {

    console.log(this.state.informacion);
    let i=1;

    let listaDeEventos = this.state.informacion.map(el => {
      return (
          <InfoEvento key={i++} elEvento={el} />
      )
    })



    return (
      <>
      
        <div className="cssMarginBotom">
          <Carrusel />
        </div>


          {listaDeEventos}
          <a  id="to_top" href="#top">Arriba</a>
          <FootBar />
       
      </>
    );
  }
}

export default Eventos;
