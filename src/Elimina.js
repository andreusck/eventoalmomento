import React from "react";
import { URL, IMGURL } from "./Utils";
import { Redirect } from "react-router-dom";

class Elimina extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        infoEvento: null,
      id: this.props.match.params.idEvento,
      volver: false
    };
    this.confirmaElimina = this.confirmaElimina.bind(this);
    this.cancelaElimina = this.cancelaElimina.bind(this);
  }

  componentDidMount() {
    fetch(URL + "evm_eventos/" + this.state.id)
      .then(data => data.json())
      .then(data => this.setState({ infoEvento: data[0]}))
      .catch(err => console.log(err));
  }

  confirmaElimina() {
    fetch(URL + "evm_eventos/" + this.state.id, { method: "DELETE" })
      .then(data => data.json())
      .then(data => console.log(data))
      .then(() => this.setState({ volver: true }))
      .catch(err => console.log(err));
  }

  cancelaElimina() {
    this.setState({ volver: true });
  }

  render() {
    if (this.state.volver) {
      return <Redirect to="/Admin" />;
    }
    if (!this.state.infoEvento) {
      return <h4>Cargando datos...</h4>;
    }
    let infoEvento = this.state.infoEvento;
    return (
      <>
        <h1>Seguro que quieres eliminar: {infoEvento.titulo} ?</h1>
        <hr />
        <button onClick={this.confirmaElimina}>SI</button>
        <button onClick={this.cancelaElimina}>NO</button>
      </>
    );
  }
}

export default Elimina;