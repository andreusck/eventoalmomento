import React, { Component } from "react";
import "leaflet/dist/leaflet.css";
import { Container, Row, Col } from "reactstrap";

import Mapa from "./Mapa";
import GeoLoc from "./GeoLoc.jsx";

const BarcelonaPos = [41.390205, 2.154007];
const MapSize = ["100%", 400];
const Zoom = 13;

const Coords = props =>
  !props.current ? (
    <h4>--</h4>
  ) : (
    <>
      <h3>
        {props.current.lat},{props.current.lon}
      </h3>
      <h4>{props.current.display_name}</h4>
    </>
  );

class Geo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      zoom: Zoom,
      current: {
        lat: BarcelonaPos[0],
        lon: BarcelonaPos[1],
        display_name: "Barcelona"
      }
    };
    this.setCurrent = this.setCurrent.bind(this);
  }

  setCurrent(current, zoom = 14) {
    this.setState({ current, zoom });
  }

  render() {
    return (
      <>
        <Container>
          <Row>
            <Col>
              <h2>Leaflet React Map</h2>
              <hr />
            </Col>
          </Row>

          <Row>
            <Col>
              <Mapa
                setCurrent={this.setCurrent}
                current={this.state.current}
                zoom={this.state.zoom}
                sizex={MapSize[1]}
                sizey={MapSize[0]}
              />
              <hr />
              <Coords current={this.state.current} />
            </Col>
            <Col>
              <GeoLoc
                current={this.state.current}
                setCurrent={this.setCurrent}
              />
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default Geo;
