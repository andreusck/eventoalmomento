import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container } from 'reactstrap';
import { URL, IMGURL } from "./Utils";
import Texto1 from "./Fotos/texto1.png"
import Texto2 from "./Fotos/texto2.png"
import MapaImagen from "./Fotos/MAPASTOPBAR.png"

class TopBar extends React.Component {

    render() {

        return (
            <>
                <nav id= "BarraSup" className="navbar navbar-expand-lg navbar- fixed-top">
                    <div className="container">
                    <Link id= "TextoBarra" className=" d-none d-md-block navbar-brand" to="/"><img src={Texto1}/></Link>
                        <Link id= "TextoBarra" className="d-block d-md-none navbar-brand" to="/"><img src={Texto2} /></Link>
                   
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item">
                                <Link className="cssTopMapa" to={"/mapamultiple"}  ><img  className="iconoMapa" src={MapaImagen}></img></Link>
                                <Link  to="/"><img className="cssCasita" src="https://i.imgur.com/wfW7cEJ.png"></img>
              {/* <span className="sr-only">(current)</span> */}
                                    </Link>
                                </li>
                               
                                
                               
                            </ul>
                       
                    </div>
                </nav>

            </>
        )
    }
}


export default TopBar;