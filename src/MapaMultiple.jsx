import React, { createRef, Component } from "react";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import {  Link } from "react-router-dom";
import Leaflet from "leaflet";
//carreguem css leaflet
import "leaflet/dist/leaflet.css";
import { URL, IMGURL } from "./Utils";
import "./estilos.css";

//definició icones, necessari en versió actual de leaflet amb react...
//si no no apareix la icona del Marker
Leaflet.Icon.Default.imagePath = "../node_modules/leaflet";
delete Leaflet.Icon.Default.prototype._getIconUrl;
Leaflet.Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png")
});

const BCN = [41.390205, 2.154007];

class MapaMultiple extends Component {
  constructor(props) {
    super(props);
    this.state = {
      informacion: []
    };
    this.cargaInfo = this.cargaInfo.bind(this);
  }

  componentDidMount() {
    this.cargaInfo();
  }
  cargaInfo() {
    console.log("cargando datos...");
    fetch(URL + "evm_eventos")
      .then(data => data.json())
      .then(data => this.setState({ informacion: data }))
      .catch(err => console.log(err));
  }

  render() {
    let i = 0;
    let marcador =<></>;
    if (this.state.informacion.length) {
      marcador = this.state.informacion.filter(el => el.lat && el.lon ).map(el => {
        return [
          <Marker key={i++} position={[el.lat, el.lon]}>
            <Popup >
              <span className="TITULO1">{el.titulo}</span>
              <br></br>
              <img
                
                className="img-fluid"
                src={el.nombreFoto}
              />
              
              <Link  to={`/Evento/${el.id}`}> <h5>Ir al Evento</h5>
              </Link>

            </Popup>
          </Marker>
        ];
      });
     }

    return (
      <>
        <Map
          // ref={this.mapRef}
          center={BCN}
          zoom={14}
          style={{ height: 500, width: "100%" }}
        >
          <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

          {marcador}
        </Map>
      </>
    );
  }
}

export default MapaMultiple;
