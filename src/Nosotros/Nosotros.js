
import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container } from 'reactstrap';
import "./nosotros.css"

class Nosotros extends React.Component {

   render() {

      return (
         <>
            {/* /Ordenado alfabet. por PRIMER APELLIDO 
                
                Saray García Castillo
                Antonio González García
                Andreu Sánchez Camí
                María Tovar Cano*/}

            <div>

            </div>
            {/* Col-3 para alinear */}
            <div className="row">

               {/* SARAY */}
               <div className="col-6">
                  <img src="https://i.imgur.com/Easn3ZB.jpg" className="imgGeneral"></img>

                  <h1 className="tit" >Saray García Castillo</h1>
                  <p className="linkedin">
                     <a href="https://www.linkedin.com/in/castillo-garcia-saray/" target="_blank" rel="noopener noreferrer"><img className="cssRedes" src="https://i.imgur.com/rsLqcns.png"></img></a>
                     /in/castillo-garcia-saray/</p>

                  <p className="MENSAJE"><img className="IconoMENSAJE" src="https://i.imgur.com/IK4cyt4.png"></img> <em>"Think twice code once!" </em> </p>
               </div>
               <br></br>

               {/* ANTONIO */}
               <div className="col-6">
                  <img src="https://i.imgur.com/gYg1MKR.jpg" className="imgGeneral"></img>


                  <h1 className="tit" >Antonio González García</h1>
                  <p className="linkedin">
                     <a href="https://www.linkedin.com/in/antoniogonzalezgarcia98/" target="_blank" rel="noopener noreferrer"><img className="cssRedes" src="https://i.imgur.com/rsLqcns.png"></img></a>
                     /in/antoniogonzalezgarcia98/</p>


                  <p className="MENSAJE"><img className="IconoMENSAJE" src="https://i.imgur.com/IK4cyt4.png"></img> <em>"Programando voy, programando vengo, por el camino no me entretengo." </em> </p>

               </div>
               <br></br>
               {/* ANDREU */}
               <div className="col-6">
                  <img src="https://i.imgur.com/apRepr9.jpg" className="imgGeneral"></img>


                  <h1 className="tit" >Andreu Sánchez Camí</h1>
                  <p className="linkedin">
                     <a href="https://www.linkedin.com/in/andreu-sck/" target="_blank" rel="noopener noreferrer"><img className="cssRedes" src="https://i.imgur.com/rsLqcns.png"></img></a>
                     /in/andreu-sck</p>

                  <p className="MENSAJE"><img className="IconoMENSAJE" src="https://i.imgur.com/IK4cyt4.png"></img> <em>"Programando mis sueños" </em> </p>
               </div>

               {/* MARÍA */}
               <div className="col-6">
                  <img src="https://i.imgur.com/k9OYhgk.jpg" className="imgGeneral"></img>


                  <h1 className="tit" >María Tovar Cano</h1>
                  <div className="cssLineaLinkedin">
                     <p className="linkedin">
                        <a className="imgIN" href="https://www.linkedin.com/in/mariatovarcano/" target="_blank" rel="noopener noreferrer"><img className="cssRedes" src="https://i.imgur.com/rsLqcns.png"></img></a>
                        /in/mariatovarcano/</p>

                     <p className="MENSAJE"><img className="IconoMENSAJE" src="https://i.imgur.com/IK4cyt4.png"></img> <em>
                        "I'm gonna need more coffee" </em> </p>
                  </div>


               </div>




            </div>


         </>
      )
   }
}


export default Nosotros;


















